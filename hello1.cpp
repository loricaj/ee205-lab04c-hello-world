
///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 04c - Hello World
///
/// @file hello1.cpp
/// @version 1.0
///
/// C++ Hello World
///
/// @author Joshua Lorica <loricaj@hawaii.edu>
/// @date   2/15/21
///////////////////////////////////////////////////////////////////////////////

#include <iostream>

using namespace std;

int main(){

   cout << "Hello World!" << endl;

   return 0;
}
