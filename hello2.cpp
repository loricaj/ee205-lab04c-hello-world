
///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 04c - Hello World
///
/// @file hello2.cpp
/// @version 1.0
///
/// C++ Hello World
///
/// @author Joshua Lorica <loricaj@hawaii.edu>
/// @date   2/15/21
///////////////////////////////////////////////////////////////////////////////

#include <iostream>

int main(){

   std::cout << "Hello World!" << std::endl;

   return 0;
}
